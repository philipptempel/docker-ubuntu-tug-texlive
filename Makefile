SHELL=/bin/sh

TEXLIVE_YEAR ?= latest
TEXLIVE_REPO ?= http://mirrors.ctan.org/systems/texlive/tlnet
UBUNTU_FLAVOR ?= focal

SCHEMES = ubuntu infraonly minimal basic small medium full
BUILDS_ALL = $(patsubst %,build-%,$(SCHEMES))
PUSHES_ALL = $(patsubst %,push-%,$(SCHEMES))
CLEANS_ALL = $(patsubst %,clean-%,$(SCHEMES))
TEST_ALL = $(patsubst %,test-%,$(SCHEMES))

# order of creation
# 0) ubuntu
# 1) infraonly
# 2) minimal
# 3) basic
# 4) small
# 5) medium
# 6) full

# ALL target
.PHONY: all
all: build push

# Build everything
.PHONY: build
build: $(BUILDS_ALL)

# Push everything
.PHONY: push
push: $(PUSHES_ALL)

# Clean up everything
.PHONY: clean
clean: $(CLEANS_ALL)

# Test everything
.PHONY: test
test: $(TEST_ALL)

# Generic targets
build-%:
	./src/maker.sh build $*

push-%:
	./src/maker.sh push $*

clean-%:
	./src/maker.sh clean $*

test-%:
	./src/test.sh $*

# scheme-specific targets
ubuntu: build-ubuntu push-ubuntu

infraonly: ubuntu build-infraonly push-infraonly

minimal: infraonly build-minimal push-minimal

basic: minimal build-basic push-basic

small: basic build-small push-small

medium: small build-medium push-medium

full: medium build-full push-full


# version-specific targets
%-build:
	$(MAKE) build
%-push:
	$(MAKE) push
%-test:
	$(MAKE) test

latest-%: export TEXLIVE_YEAR := latest
latest-%: export TEXLIVE_REPO := http://mirrors.ctan.org/systems/texlive/tlnet
latest-%: export UBUNTU_FLAVOR := jammy
.PHONY: latest
latest: latest-build latest-test latest-push

2022-%: export TEXLIVE_YEAR := 2022
2022-%: export TEXLIVE_REPO := ftp://tug.org/historic/systems/texlive/2022/tlnet-final
2022-%: export UBUNTU_FLAVOR := jammy
.PHONY: 2022
2022: 2022-build 2022-test 2022-push

2021-%: export TEXLIVE_YEAR := 2021
2021-%: export TEXLIVE_REPO := ftp://tug.org/historic/systems/texlive/2021/tlnet-final
2021-%: export UBUNTU_FLAVOR := focal
.PHONY: 2021
2021: 2021-build 2021-test 2021-push

2020-%: export TEXLIVE_YEAR := 2020
2020-%: export TEXLIVE_REPO := ftp://tug.org/historic/systems/texlive/2020/tlnet-final
2020-%: export UBUNTU_FLAVOR := focal
.PHONY: 2020
2020: 2020-build 2020-test 2020-push

2019-%: export TEXLIVE_YEAR := 2019
2019-%: export TEXLIVE_REPO := ftp://tug.org/historic/systems/texlive/2019/tlnet-final
2019-%: export UBUNTU_FLAVOR := bionic
.PHONY: 2019
2019: 2019-build 2019-test 2019-push

2018-%: export TEXLIVE_YEAR := 2018
2018-%: export TEXLIVE_REPO := ftp://tug.org/historic/systems/texlive/2018/tlnet-final
2018-%: export UBUNTU_FLAVOR := bionic
.PHONY: 2018
2018: 2018-build 2018-test 2018-push

2017-%: export TEXLIVE_YEAR := 2017
2017-%: export TEXLIVE_REPO := ftp://tug.org/historic/systems/texlive/2017/tlnet-final
2017-%: export UBUNTU_FLAVOR := xenial
.PHONY: 2017
2017: 2017-build 2017-test 2017-push

2016-%: export TEXLIVE_YEAR := 2016
2016-%: export TEXLIVE_REPO := ftp://tug.org/historic/systems/texlive/2016/tlnet-final
2016-%: export UBUNTU_FLAVOR := xenial
.PHONY: 2016
2016: 2016-build 2016-test 2016-push
